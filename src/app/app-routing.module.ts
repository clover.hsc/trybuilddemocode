import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  { path: 'home', loadChildren: ()=> import('./home/home.module').then(m=>m.HomeModule)},
  { path: 'contact', loadChildren: ()=> import('./contact/contact.module').then(m=>m.ContactModule)},
  { path: 'productions', loadChildren: () => import('./productions/productions.module').then(m=>m.ProductionsModule)},
  { path: 'service', loadChildren: () => import('./service/service.module').then(m=>m.ServiceModule)},
  { path: 'login', component: LoginComponent},
  { path: '', redirectTo: 'login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
