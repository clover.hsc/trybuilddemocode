import { Component, OnInit } from '@angular/core';

import * as $ from 'jquery';
import * as d3 from 'd3';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('#myBtn').css('border', '3px solid red');

    const svg = d3.select('#Viz_area');

    const x = d3
    .scaleLinear()
    .domain([0, 100])
    .range([0, 400]);

    svg.call(d3.axisBottom(x));

    svg.append('circle').attr('cx', x(10)).attr('cy', 100).attr('r', 40)
    .style('fill', 'blue');
    svg.append('circle').attr('cx', x(50)).attr('cy', 100).attr('r',40)
    .style('fill', 'red');
    svg.append('circle').attr('cx', x(100)).attr('cy', 100).attr('r', 40)
    .style('fill', 'green');
  }

}
